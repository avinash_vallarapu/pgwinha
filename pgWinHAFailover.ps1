#### Push Failover of PgBouncers ######
#### Modification 1 : 
#### -----------------
####		Create an AppServers.csv File. Make sure that the File Type is .csv  
####               It must contain the list of App Server where PgBouncer is running.
####	           Line Number 1 must be header --> "Hostname". See the following example.
####		   
####	            "Hostname"
####	            "AppServer1"
####		    "AppServer2"
####		    ".........."
####		    "AppServern"
####         Now, put the location and File Name in $Hosts after Import-Csv
####
#### Modification 2 :
#### -----------------
#### 		Substitue the Actual Service Name of PgBouncer which is connecting to Original Master
#### 		 $ServiceName	
####
#### Modification 3 :
#### -----------------
####		Substitue the Failover Service Name of PgBouncer which should be used for Failover to Slave.
#### 		 $FailoverServiceName	
####
#### Script Body starts now.
#### 
$Hosts = Import-Csv "C:\Users\Administrator\Desktop\AppServers.csv"
$ServiceName = "pgbouncer"
$FailoverServiceName = "pgbouncer_failover"
#### Function to Check the Status of Service Mentioned for variable ServiceName
function checkOriginalStatus()
{
	$OrigServiceState = Get-service $ServiceName -ComputerName $Hostname
	write-host ("===========================================================================")
	write-host ("Service $ServiceName is") $OrigServiceState.status on $Hostname
	$FailoverServiceState = Get-service $FailoverServiceName -ComputerName $Hostname
	write-host ("Service $FailoverServiceName is") $FailoverServiceState.status on $Hostname
	write-host ("===========================================================================")
}
#### Function to Check the Status of Service Mentioned for variable FailoverServiceName
function checkFailoverStatus()
{
	$FailoverServiceState = Get-service $FailoverServiceName -ComputerName $Hostname
	write-host ("===========================================================================")
	write-host ("Service $FailoverServiceName is") $FailoverServiceState.status on $Hostname
	$OrigServiceState = Get-service $ServiceName -ComputerName $Hostname
	write-host ("Service $ServiceName is") $OrigServiceState.status on $Hostname
	write-host ("===========================================================================")
}
## Function to Stop the Original Service Mentioned for Variable ServiceName
function StopOriginalService()
{
Stop-Service -InputObject $(Get-Service -Computer $Hostname -Name $ServiceName)
}
## Function to Start the Service Mentioned for Variable FailoverServiceName
function Startfailover()
{
	Start-Service -InputObject $(Get-Service -Computer $Hostname -Name $FailoverServiceName)
	$ServiceState = Get-service $FailoverServiceName -ComputerName $Hostname
	write-host ("Service $FailoverServiceName is") $ServiceState.status on $Hostname
}
### Main 
foreach ($app in $Hosts)
{
$Hostname = $app.Hostname;
write-host ("Entering $Hostname")
checkOriginalStatus;
StopOriginalService;
checkFailoverStatus;
$OrigServiceState = Get-service $ServiceName -ComputerName $Hostname
	if ($OrigServiceState.status -eq 'Stopped' -or $OrigServiceState.status -eq $null)
		{
			write-host ("Continuing to Failover as Service $ServiceName is") $OrigServiceState.status on $Hostname;
			Startfailover;
		}
	Else
		{
			write-host ("Failover is Haulted as Service $ServiceName is") $OrigServiceState.status on $Hostname;
			write-host ("Please do a Manual Stop of $ServiceName and Manual Start of Services $FailoverServiceName on your App Server ") on $Hostname;
		}
checkFailoverStatus;
write-host ("Exiting $Hostname")
}