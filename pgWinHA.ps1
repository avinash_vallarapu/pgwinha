#### Push Failback or Reset of PgBouncers to Original Configs ######
#### Modification 1 : 
#### -----------------
####		Create an AppServers.csv File. Make sure that the File Type is .csv  
####               It must contain the list of App Server where PgBouncer is running.
####	           Line Number 1 must be header --> "Hostname". See the following example.
####		   
####	            "Hostname"
####	            "AppServer1"
####		    "AppServer2"
####		    ".........."
####		    "AppServern"
####         Now, put the location and File Name in $Hosts variable after Import-Csv. See the First line of the Script Body.
####
#### Modification 2 :
#### -----------------
#### 		Substitue the Actual Service Name of PgBouncer which is connecting to Original Master
#### 		 $ServiceName	
####
#### Modification 3 :
#### -----------------
####		Substitue the Failover Service Name of PgBouncer which should be used for Failover to Slave.
#### 		 $FailoverServiceName	
####
#### Script Body starts now.
#### 
$Hosts = Import-Csv "C:\Users\Administrator\Desktop\AppServers.csv"
$ServiceName = "pgbouncer"
$FailoverServiceName = "pgbouncer_failover"
#### Function to Check the Status of Service Mentioned for variable ServiceName
function checkOriginalStatus()
{
	$OrigServiceState = Get-service $ServiceName -ComputerName $Hostname
	write-host ("===========================================================================")
	write-host ("Service $ServiceName is") $OrigServiceState.status on $Hostname
	$FailoverServiceState = Get-service $FailoverServiceName -ComputerName $Hostname
	write-host ("Service $FailoverServiceName is") $FailoverServiceState.status on $Hostname
	write-host ("===========================================================================")
}
#### Function to Check the Status of Service Mentioned for variable FailoverServiceName
function checkFailoverStatus()
{
	$FailoverServiceState = Get-service $FailoverServiceName -ComputerName $Hostname
	write-host ("===========================================================================")
	write-host ("Service $FailoverServiceName is") $FailoverServiceState.status on $Hostname
	$OrigServiceState = Get-service $ServiceName -ComputerName $Hostname
	write-host ("Service $ServiceName is") $OrigServiceState.status on $Hostname
	write-host ("===========================================================================")
}
## Function to Stop the Original Service Mentioned for Variable ServiceName
function StopOriginalService()
{
Stop-Service -InputObject $(Get-Service -Computer $Hostname -Name $ServiceName)
}
function StopFailoverService()
{
Stop-Service -InputObject $(Get-Service -Computer $Hostname -Name $FailoverServiceName)
}
## Function to Start the Service Mentioned for Variable FailoverServiceName
function Startfailover()
{
	Start-Service -InputObject $(Get-Service -Computer $Hostname -Name $FailoverServiceName)
	$ServiceState = Get-service $FailoverServiceName -ComputerName $Hostname
	write-host ("Service $FailoverServiceName is") $ServiceState.status on $Hostname
}
function Startfailback()
{
	Start-Service -InputObject $(Get-Service -Computer $Hostname -Name $ServiceName)
	$ServiceState = Get-service $ServiceName -ComputerName $Hostname
	write-host ("Service $ServiceName is") $ServiceState.status on $Hostname
}
### Main 
foreach ($app in $Hosts)
{
$Hostname = $app.Hostname;
write-host ("Entering $Hostname")
checkFailoverStatus;
StopFailoverService;
checkFailoverStatus;
$FailoverServiceState = Get-service $FailoverServiceName -ComputerName $Hostname
	if ($FailoverServiceState.status -eq 'Stopped' -or $OrigServiceState.status -eq $null)
		{
			write-host ("Continuing to Failover as Service $FailoverServiceName is") $OrigServiceState.status on $Hostname;
			Startfailback;
		}
	Else
		{
			write-host ("Failback is Haulted as Service $FailoverServiceName is") $FailoverServiceState.status on $Hostname;
			write-host ("Please do a Manual Stop of $FailoverServiceName and Manual Start of Services $ServiceName on your App Server ") on $Hostname;
		}
checkOriginalStatus;
write-host ("Exiting $Hostname")
}